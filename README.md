# Ubuntu Touch

## A simple and beautiful mobile OS for everyone!
[Ubuntu Touch](https://ubports.com) is the touch-friendly mobile version of the [Ubuntu](https://ubuntu.com) operating system. Originally designed and developed by Canonical, it now lives on in the UBports community.

![Ubuntu Touch](https://gitlab.com/ubports/marketing/mplus/marketing-materials/-/raw/master/Brandbook/contentpack/logos/logo_ubuntutouch/statement_powered_by_UBPORTS_foundation/png/logo-ubuntutouch-01.png?ref_type=heads)

# File a Bug or Feature for your Device

Dear Ubuntu Touch user if you want to file a bug or a feature request please select your device in the list below. When you can't find your device please search for it in the search box right side above.

Please read our [guide on writing a good bug report](https://docs.ubports.com/en/latest/contribute/bugreporting.html) before creating a new issue. To better understand the lifecycle of your issue, refer to our [issue tracking guidelines](https://docs.ubports.com/en/latest/about/process/issue-tracking.html). Thank you!

### Labels and Filtering

You can use Gitlab's filtering syntax to reduce clutter. [Here's a brief explaination of the standard labels and what they mean](https://docs.ubports.com/en/latest/about/process/issue-tracking.html#labels).


## Devices

- [Fairphone 4](https://gitlab.com/groups/ubports/porting/reference-device-ports/android11/fairphone-4/-/issues)
- [Volla Phone 22](https://gitlab.com/groups/ubports/porting/reference-device-ports/android11/volla-phone-22/-/issues)

#### [PORTERS PLEASE ADD YOUR DEVICE REPO LINK TO THIS LIST]
